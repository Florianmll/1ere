# Exercices : codage des caractères



## Exercice 1 - Décodage ASCII

La table ci-dessous donne le code associé à chacun des caractères ASCII imprimables.
Le code du caractère en hexadécimal s'obtient  en écrivant le numéro de la ligne
suivi du numéro de la colonne. Par  exemple, la lettre M a pour code hexadécimal
`4D`, c'est à dire `77` en décimal.

|     | 0      | 1   | 2   | 3   | 4   | 5   | 6   | 7   | 8   | 9   | A   | B   | C   | D   | E   | F   |
|:---:|:------:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
| 0   |        |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| 1   |        |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| 2   | espace | !   | "   | #   | $   | %   | &   | '   | (   | )   | *   | +   | ,   | -   | .   | /   |
| 3   | 0      | 1   | 2   | 3   | 4   | 5   | 6   | 7   | 8   | 9   | :   | ;   | <   | =   | >   | ?   |
| 4   | @      | A   | B   | C   | D   | E   | F   | G   | H   | I   | J   | K   | L   | M   | N   | O   |
| 5   | P      | Q   | R   | S   | T   | U   | V   | W   | X   | Y   | Z   | [   | \   | ]   | ^   | __  |
| 6   | `      | a   | b   | c   | d   | e   | f   | g   | h   | i   | j   | k   | l   | m   | n   | o   |
| 7   | p      | q   | r   | s   | t   | u   | v   | w   | x   | y   | z   | {   | |   | }   | ~   |     |


À quel nom correspond la liste des codes ASCII suivants, donnés en hexadécimal ?

~~~
47 65 6F 72 67 65 20 42 6F 6F 6C 65
~~~

## Exercice 2 - Décodage UTF-8 

En UTF-8, le codage des caractères coïncide avec l'ASCII pour les 128 premiers caractères (table donnée plus haut). Les autres caractères sont représentés par plusieurs octets.

La série d'octets suivants, donnés en hexadécimal, a été relevée dans un fichier codé en UTF-8.

~~~
43 6F 64 C3 A9 20 65 6E 20 55 54 46 2D 38
~~~

Il contient uniquement des caractères de la table ASCII ainsi qu'un « é ». Quelle est la séquence d'octets qui représente le « é », et qu'est ce qui est inscrit dans le fichier ?

Si le fichier avait été interprété en latin 1 (table ci-dessous), qu'est ce qui se serait affiché ?

|       | 0   | 1   | 2   | 3   | 4   | 5   | 6   | 7   | 8   | 9   | A   | B   | C   | D   | E   | F   |
|:-----:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
| **2** |     | !   | "   | #   | $   | %   | &   | '   | (   | )   | *   | +   | ,   | -   | .   | /   |
| **3** | 0   | 1   | 2   | 3   | 4   | 5   | 6   | 7   | 8   | 9   | :   | ;   | <   | =   | >   | ?   |
| **4** | @   | A   | B   | C   | D   | E   | F   | G   | H   | I   | J   | K   | L   | M   | N   | O   |
| **5** | P   | Q   | R   | S   | T   | U   | V   | W   | X   | Y   | Z   | [   | \   | ]   | ^   | _   |
| **6** | `   | a   | b   | c   | d   | e   | f   | g   | h   | i   | j   | k   | l   | m   | n   | o   |
| **7** | p   | q   | r   | s   | t   | u   | v   | w   | x   | y   | z   | {   |     | }   | ~   |     |
| **8** |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| **9** |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| **A** |     | ¡   | ¢   | £   | ¤   | ¥   | ¦   | §   | ¨   | ©   | ª   | «   | ¬   | -   | ®   | ¯   |
| **B** | °   | ±   | ²   | ³   | ´   | µ   | ¶   | ·   | ¸   | ¹   | º   | »   | ¼   | ½   | ¾   | ¿   |
| **C** | À   | Á   | Â   | Ã   | Ä   | Å   | Æ   | Ç   | È   | É   | Ê   | Ë   | Ì   | Í   | Î   | Ï   |
| **D** | Ð   | Ñ   | Ò   | Ó   | Ô   | Õ   | Ö   | ×   | Ø   | Ù   | Ú   | Û   | Ü   | Ý   | Þ   | ß   |
| **E** | à   | á   | â   | ã   | ä   | å   | æ   | ç   | è   | é   | ê   | ë   | ì   | í   | î   | ï   |
| **F** | ð   | ñ   | ò   | ó   | ô   | õ   | ö   | ÷   | ø   | ù   | ú   | û   | ü   | ý   | þ   | ÿ   |






## Exercice 3 - Lecture de tables

À quels caractères correspondent les codes ASCII suivants ?

1. `1000001` (binaire)
2. `118` (décimal)
3. `7D` (hexadécimal)

À quels caractères correspondent les codes ISO-8859-1 suivants ?

1. `01000001` (binaire)
2. `118` (décimal)
3. `A9` (hexadécimal)

À quels caractères correspondent les points de code unicode suivants ?

1. `41` (hexadécimal)
2. `A9` (hexadécimal)
3. `20AC` (hexadécimal)
4. `424` (hexadécimal)
5. `2620` (hexadécimal)


## Exercice 4 - Décodge UTF8


Les séquences  d’octets suivantes  (données en hexadécimal)  sont des  débuts de
textes  encodés en  UTF-8.  Pour  chacune d’entre  elles,  indiquez sur  combien
d’octets est  codé le premier  caractère, puis donnez  le point de  code unicode
correspondant.

1. `0A EA 8C A2`
2. `CD AD CD B4`
3. `7A E7 8F 9F`
4. `E1 8F A9 00`
5. `CB A9 01 5A`
6. `DD AA CD B3`
7. `4A EA AA AA`
8. `EA 99 88 12`
9. `31 32 C8 AA`
10.`14 25 CA AB`
11. `55 E2 AB 98`
12. `6A 7B CC BB`
13. `D9 99 05 77`
14. `DC AA 45 67`
15. `E6 A6 B6 02`
16. `22 23 34 77`
17. `CF A2 CA B8`


## Exercice 5 - Encodage UTF8

Encodez en UTF-8 les points de code de l'exercice 1.

## Exercice 6 - ISO-8859-1

Le texte suivant a été encodé en ISO-8859-1, décodez-le.

`01000001011101000111010001100101011011100111010001101001011011110110111`
`00010110000100000011000110110010100100000011011010110010101110011011100`
`11011000010110011101100101001000000110000100100000011001010110111000100`
`00001100110011000010110100101110100001000001100001110101001011101001100`
`00111010100100100000011001010110111001100011011011110110010011000011101`
`01001001000000110010101101110001000000101010101010100010001100010110100`
`11100000100000001000010000101000001010010101010110111000100000011100000`
`11000010110111001100111011100100110000101101101011011010110010100100000`
`00111010001000001100001010101011110000101010000001110110011011110110100`
`10111100000100000011000010110110101100010011010010110011101110101110000`
`11101010110010000001100100001001110111010101101110001000000110001111000`
`10110010011011101010111001000100000011100010111010101101001001011000010`
`00000110000101110101001000000111101011000011101010010111000001101000011`
`11001011100100010110000100000011100000111001011000011101010010110011011`
`00001110101000011100100110010100100000011011000110010101110011001000000`
`11010100110000101110100011101000110010101110011001000000110010001100101`
`00100000011010110110100101110111011010010111001111000010101000001100001`
`01011101100101110000010100000101001000101011011100110001101101111011001`
`00011001010111101000100000011011000110000100100000011100000110100001110`
`01001100001011100110110010100100000011001000010011101101001011011100111`
`01000111001001101111011001000111010101100011011101000110100101101111011`
`01110001000000110010001100101001000000110001101100101011101000010000001`
`10010101111000011001010111001001100011011010010110001101100101001000000`
`11001010110111000100000010101010101010001000110001011010011100000101110`


## Exercice 5 - Gros défi !

L'objectif de ce défi est d’écrire un petit programme C qui soit capable de coder et décoder du texte en
UTF8.  Ce programme,  que  nous  nommerons utf8,  se  comportera  de la  manière
suivante :

* si on l'appelle avec pour argumentr#s `t` et un texte, il affichera le codage de ce texte en UTF8
(représenté en hexadécimal),
* si on l'appelle avec pour arguments `r` et une séquence d'octets représentés en héxadécimal, il
considérera cette  séquence comme le codage  d'un texte en UTF8  et affichera ce
texte en clair.

```python
>>> utf8(t,"bonjour")
Le codage de "bonjour" donne "62 6F 6E 6A 6F 75 72"

>>> utf8(r,"62 6f 6e 6a 6f 75 72")
Le décodage de "62 6f 6e 6a 6f 75 72" donne "bonjour"

>>> utf8(t, "ÀÉÈËÇ")
Le codage de "ÀÉÈËÇ" donne "C3 80 C3 89 C3 88 C3 8B C3 87"

>>> utf8(r, "C3 80 C3 89 C3 88 C3 8B C3 87")
Le décodage de "C3 80 C3 89 C3 88 C3 8B C3 87" donne "ÀÉÈËÇ"
```
