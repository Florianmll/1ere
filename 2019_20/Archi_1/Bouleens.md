% Algèbre booléenne

# Repères historiques

À  partir de  1847, le  britannique  *George BOOLE*  propose un  mode de  calcul
permettant   de  traduire   des  raisonnements   logiques  par   des  opérations
algébriques.

En  1938,  l'américain *Claude  SHANNON*  prouve  que des  circuits  électriques
peuvent  résoudre tous  les  problèmes  que l'algèbre  de  Boole peut  elle-même
résoudre.  Avec  les  travaux  d'*Alan  TURING*  de  1936,  cela  constitue  les
fondements de ce qui deviendra l'informatique.

# Algèbre de Boole

C'est une branche  des mathématiques qui définit des opérations  dans un ensemble
qui ne contient que **deux éléments** notés 0 et 1, ou bien FAUX et VRAI ou
encore sur Python `False` et `True` pour ne pas les confondre avec les *entiers*
0 et 1. Les opérations fondamentales  ne sont plus l'addition et la multiplication
mais la *conjonction* (notée `&` et lue  ET), la *disjonction* (notée `|` et lue
OU) et la *négation* (notée `~` et lue NON).

Dans  toute la  suite,  $`x`$ et  $`y`$ dénoteront  des  *Booléens* (éléments  d'une
algèbre de Boole) quelconques, `F` désignera FAUX et `V` désignera VRAI.

## Conjonction

C'est l'opération définie par:

* `x & F = F`
* `x & V = x`

Puisque l'algèbre de  Boole ne contient que deux éléments,  on peut étudier tous
les cas possibles et les regrouper dans un tableau appelé **table de vérité**:


|`x`| `y` | `x & y`|
|:--:|:----:|:--:|
|F|F| F|
|F|V|F|
|V|F|F|
|V|V|V|


On représente souvent les opérateurs booléens à l'aide de portes logiques:


![](IMG/porte_et.png)

## Disjonction 

C'est l'opération définie par:

* `x | V = V`
* `x | F = x`

On en déduit la table suivante:



|`x`| `y` | `x \| y`|
|:--:|:----:|:--:|
|F|F| F|
|F|V|V|
|V|F|V|
|V|V|V|



![](IMG/porte_ou.png)


## Négation


C'est l'opération définie par:

* `~V = F`
* `~F = V`

On en déduit la table suivante:


|`x`| `~x` |
|:--:|:----:|
|F|V|
|V|F|

![](IMG/porte_non.png)



# Lois 

Les lois suivantes sont facilement démontrables à l'aide de tables de vérités:


|Propriété | Signification|
|:---------|:------------:|
|Commutativité|`x \| y = y \| x` et `x & y = y & x`|
|Associativité|`x \| (y \| z) = (x \| y) \| z` et `x & (y & z) = x & (y & z)`|
|Distributivité|`x & (y \| z) = (x & y) \| (x & z)` et `x \| (y & z) = (x \| y) & (x \| z)`|
|Élément neutre|`x \| F = x` et `x & V = x`|
|Élément absorbant|`x & F = F`|
|Involution|`~(~x) = x`|
|Tiers-exclus|`~x \| x = V`|
|Non-contradiction|`~x & x = F`|
|Idempotence|`x&x=x` et `x \| x = x`|
|Lois de De Morgan|`~(x \| y) = ~x & ~y` et `~(x & y) = ~x \| ~y`|


## En Python

Les  booléens  `V`  et `F`  se  notent  `True`  et  `False` mais  aussi  `1`  et
`0`. Les opérateurs sont `or` (disjonction), `and` (conjonction) et `not` (négation):

``` Python
>>> not(True or False) == (not True) and (not False)
True

>>> not(1 or 0) == (not 1) and (not 0)
True
```

L'opérateur  `==` permet  de tester  si  deux références  contiennent les  mêmes
valeurs en renvoyant un booléen.

# Fonctions composées

## Disjonction exclusive

`x ^ y = (x & ~y) | (~x & y)`


|`x`| `y` | `x ^ y`|
|:--:|:----:|:--:|
|F|F| F|
|F|V|V|
|V|F|V|
|V|V|F|



![](IMG/porte_xor.png)



## Non Et

`x ↑ y = ~(x & y)`


|`x`| `y` | `x ↑ y`|
|:--:|:----:|:--:|
|F|F| V|
|F|V|V|
|V|F|V|
|V|V|F|



![](IMG/porte_nand.png)



## Non Ou


`x ↓ y = ~(x & y)`


|`x`| `y` | `x ↓ y`|
|:--:|:----:|:--:|
|F|F| V|
|F|V|F|
|V|F|F|
|V|V|F|



![](IMG/porte_nor.png)

# Opérations bit à bit

On peut généraliser les opérations précédentes à des chaînes de bits. 

Par exemple:

```
   1011011
&  1010101
----------
   1010001
   
   1011011
|  1010101
----------
   1011111
   
   1011011
^  1010101
----------
   0001110
```

## Avec Python

```Python
>>> bin(0b1011011 & 0b1010101)
'0b1010001'

>>> bin(0b1011011 | 0b1010101)
'0b1011111'

>>> bin(0b1011011 ^ 0b1010101)
'0b1110'
```




# EXERCICES



## Exercice 1 - Opérations bit à bit basiques 

Complétez le tableau  suivant en évaluant les opérations proposées  à partir des
octets $`x`$ et $`y`$ fournis:

|Propriété | Signification|
|:---------|:------------:|
| `x` | `01101001`|
| `y` | `01010101`|
| `x & y` | |
| `x \| y` | | 
| `x ^ y`| |
| `x ↑ y`| |

## Exercice 2 - Lois de De Morgan

À l'aide d'une table de vérité, démontrez les lois de De Morgan


## Exercice 3 - Couleurs RVB et opérations bits à bits 

Sur machine,  les couleurs sont  par exemple créées  en mélangeant du  **R**ouge, du
**V**ert et du **B**leu. On peut imaginer un dispositif dans lequel trois lampes
de chacune de ces couleurs sont dirigées vers le même endroit et peuvent être allumées ou éteintes. 

![](IMG/RGB.png)


1. Vérifier que l'on peut alors créer 8 couleurs différentes.
2. Voici leurs noms:

|R|V|B|Couleur|
|:-:|:-:|:-:|:--:|
|0|0|0|Noir|
|0|0|1|Bleu|
|0|1|0|Vert|
|0|1|1|Cyan|
|1|0|0|Rouge|
|1|0|1|Magenta|
|1|1|0|Jaune|
|1|1|1|Blanc|

Le complément  d'une couleur est  obtenu en allumant  les lampes éteintes  et en
éteignant les lampes allumées. Déterminez  les couleurs complémentaires des huit
couleurs précédentes.

3. Quelle est la couleur obtenue en effectuant les opérations suivantes:
* `Bleu | Rouge`
* `Magenta & Cyan`
* `Vert ^ Blanc`


## Exercice 4 - Masques et couleurs du Web 

En HTML et en CSS, les couleurs  sont le plus souvent mémorisées sous forme d'un
triplet  hexadécimal:  c'est un  nombre  écrit  en base  16  d'une  taille de  3
octets. Chaque octet représente donc un niveau parmi 256 de chacune des trois couleurs Rouge,
Vert et Bleu et est écrit en hexadécimal. 


Dans `#0000FF`, mettre les 2 premiers chiffres en rouge, les 2 suivants en vert et les 2 derniers en bleu

Par  exemple  `#0000FF` 
représente le bleu (niveau nul en rouge et vert et maximal (FF = 255) en bleu).


De même, `#FF00FF` 
représente le magenta d'après l'exercice précédent.

On voudrait pouvoir récupérer chacune de ces trois paires d'octets afin d'écrire
la couleur  sous la forme  d'un triplet  `(r,v,b)` où `r`,  `v` et `b`  sont des
entiers entre 0 et 255.

1. Soit c le triplet hexadécimal  d'une couleur. Expliquez pourquoi il suffit de
   calculer `c & 11111111` pour obtenir son niveau de bleu.
   
2.  L'opérateur `>>`  permet  de  décaler l'écriture  binaire  d'un nombre  d'un
   certain nombre de rangs vers la droite: 
   
   $`b_7  b_6 b_5 b_4 b_3 b_2 b_1 b_0 >> 2 = b_7 b_6 b_5 b_4 b_3 b_2`$
   
   
   
   Par exemple `101101 >> 2 = 1011`.

  Utilisez ces décalages pour obtenir les niveaux de vert et de rouge.



## Exercice 5 - OUEX

 Le connecteur$`⨁`$ est défini par:
 $`1 ⨁ 1=0`$, $` 1 ⨁ 0=1`$, $`0 ⨁ 1=1`$ et $` 0 ⨁ 0 = 0`$.

1. Simplifier:
  * $`x ⨁ 0`$ 
  * $`x ⨁ 1`$
  * $` x ⨁ x`$ 
  * $` x ⨁ ~x`$


2. Montrer que:


  * $`x ⨁ y =\left(x | y\right) \& ¬\left(x \& y\right)`$
  * $` x ⨁ y =\left(x \& ¬y\right) | \left(¬x \& y\right)`$


3. L'opération $`⨁`$ est-elle commutative?

4. Vrai ou faux?
  * $` x ⨁ \left(y⨁ z\right) =\left(x ⨁ y\right)⨁ z`$
  * $`x |\left(y⨁ z\right) = (x | y) ⨁ (x | z)`$
  * $`x ⨁ (y | z) =(x ⨁ y) | (x⨁ z)`$


## Exercice 6 - Formules logiques et circuits logiques

Pour chacune des  formules suivantes, dessiner un circuit logique  ayant la même
table de vérité.
On
utilisera uniquement les portes et, ou, non-et, non, xor. Après chaque formule est indiqué entre parenthèse
le nombre de portes maximum à utiliser pour construire le circuit correspondant.

On note souvent `&` par `.`, `|` par `+` et `~` par `¬`.

1. `x.y` (une porte maximum) 
2. `x + y` (une porte maximum)
3. `¬x` (une porte maximum)
4. `¬(x.y)` (une porte maximum)
5. `(x + y).¬(x.y)` (une porte maximum)
6. `x.y + z` (deux portes maximum)
7. `(x.y) + (x.z) + (y.z)` (cinq portes maximum)
8. `¬(¬(x.y) + z.t)` (cinq portes maximum)
9. `(x+¬y).(z +¬(¬t.u))` (sept portes maximum)

## Exercice 7 - Décalages

Étant donnée une séquence de $`k`$  bits, une opération classique consiste à décaler
cette  séquence de  bits de  $`n`$ crans  vers la  gauche. On  obtient une  nouvelle
séquence de $`k`$ bits  dont les $`k − n`$ premiers sont les  $`k − n`$ derniers
   de la séquence initiale et dont les $`n`$ derniers sont des 0. Cette opération s’appelle
un décalage logique à gauche.
1. Rappeler l’utilité du décalage logique à gauche.
2. Proposer un circuit logique réalisant le décalage logique à gauche avec $`n = 1`$
   et $`k = 4`$.
3. Proposer  un circuit  logique qui  a deux  entrées ($`c_1`$  et $`c_0`$ )  et quatre
   sorties ($`s_0`$ , $`s_1`$ , $`s_2`$ et $`s_3`$ ) telles
que $`s_i = 1`$ si et seulement si $`c_1 c_0`$ représente en base deux le nombre $`i`$.
4.  Déduire des  deux  questions  précédentes un  circuit  logique réalisant  le
   décalage à gauche avec $`k = 4`$
et $`n \in \{0, 1,  2, 3\}`$ contrôlé par deux entrées $`c_1`$ et $`c_0`$ .  Par des techniques
similaires  on peut  aussi réaliser  des décalages  à droite,  qui peuvent  être
logiques (on met  à 0 les bits  laissés vides par le  décalage) ou arithmétiques
(on met à la valeur du bit de  poids fort de l’entrée les bits laissés vides par
le décalage).
1. Comment réaliseriez vous des circuits logiques pour ces décalages ?
2. À votre avis, pourquoi existe-t-il deux décalages à droite différents ?

## Exercice 8 - Comptage du nombre de bits à 1 en utilisant les opérateurs bit à bit
   
   
   Écrivez un  programme Python  qui compte le  nombre de bits  à 1  d'un nombre
   donné en argument en utilisant uniquement les opérateurs `&`, `>>` et `+`.
   
   
## Exercice 9 - Calculs

Donnez les résultats des expressions suivantes :

* `0xFF00 FF00 & 0x12FF 34FF`
* `0xFF00 FF00 | 0x12FF 34FF`
* `0xFF00 FF00 ˆ 0x12FF 34FF`
* `0xFF00 FF00 << 4`
* `0xFF00 FF00 >> 4`


## Exercice 10 - Opération sur les bits

1. Donnez l’expression permettant de positionner
à 0 le bit de rang 31 d’une donnée x de taille 32 bits sans modifier les autres bits de x. Vous
donnerez deux versions : une avec un masque et l’autre sans masque.

2.  Donnez l’expression
permettant de positionner à 0 les bits de rang 30 à 23 d’une donnée x de taille 32 bits sans
modifier les autres bits de x.

3.  Donnez l’expression permettant de positionner dans les bits de
rang 7 à 0 d’une donnée x de taille 32 bits, les bits de rangs 30 à 23 d’une donnée y. Vous
donnerez deux versions : une avec un masque et l’autre sans masque.

4. Écrivez des fonctions Python effectuant ces opérations.
